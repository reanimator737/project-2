**Demo**: https://reanimator737.github.io/Fork/

**Developers:**
1. Kairtsev Alexandr (made a header and sections 'fork-app', 'People talk about a fork', set up the build of the project)
2. Yukhimets Anton (made sections: "fork-suncribe", "rev-editor", "here-w-y-get")

**To launch project follow procedure:**
1. gulp build
2. gulp dev

**In project we use:**
"browser-sync",
"gulp",
"gulp-autoprefixer",
"gulp-clean",
"gulp-clean-css",
"gulp-concat",
"gulp-imagemin",
"gulp-js-minify",
"gulp-notify",
"gulp-plumber",
"gulp-sass",
"jquery".
